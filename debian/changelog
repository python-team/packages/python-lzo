python-lzo (1.15-2) unstable; urgency=medium

  * Team upload.
  * Add missing build-dep on python3-setuptools (Closes: #1080756)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 11 Oct 2024 13:17:58 +0200

python-lzo (1.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.15
  * Use new dh-sequence-python3
  * Replace nose by pytest (Closes: #1018528)
  * Replace old nose patch with PYBUILD_TEST_ARGS
  * Fix truncated description (Closes: #911838)

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Sat, 11 May 2024 17:41:41 +0200

python-lzo (1.14-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.6.0.
  * Rules-Requires-Root: no.
  * Updated upstream Homepage.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 03 Apr 2022 19:10:22 +1000

python-lzo (1.12-4) unstable; urgency=medium

  [ Stefano Rivera ]
  * Team upload.
  * Patch: Enable PY_SSIZE_T_CLEAN for Python 3.10 support. (Closes: #999380)

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Apply multi-arch hints.
    + python3-lzo: Add Multi-Arch: same.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Fri, 19 Nov 2021 15:39:04 -0400

python-lzo (1.12-3) unstable; urgency=medium

  * Team upload.
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * d/watch: Use https protocol
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).

 -- Ondřej Nový <onovy@debian.org>  Thu, 08 Aug 2019 13:19:16 +0200

python-lzo (1.12-2) unstable; urgency=medium

  * Introduced "python3-lzo" package (Closes: #898206).
  * Build-Depends:
    + python3-all-dev
    + python3-nose

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 23 Jul 2018 14:21:33 +1000

python-lzo (1.12-1) unstable; urgency=medium

  * New upstream release.
  * Build-Depends += "python-nose".
  * Build using pybuild.
  * Vcs URLs to Salsa.
  * Updated watch file.
  * Updated Homepage URL.
  * Standards-Version: 4.1.5.
  * debhelper & compat to version 11.
  * Testsuite: autopkgtest-pkg-python.
  * Set Python Team as Maintainer; Added myself to Uploaders.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 23 Jul 2018 14:09:41 +1000

python-lzo (1.08-1) unstable; urgency=low

  * Initial import (Closes: #671375)
  * add patches/01-use-lzo2-insteadof-lzo.patch to use lzo2 instead of lzo
  * add patches/02-fix-exception-handling-in-test.patch

 -- Mehdi Abaakouk <sileht@sileht.net>  Mon, 07 May 2012 19:31:01 +0200
